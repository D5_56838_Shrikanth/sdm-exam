CREATE TABLE Movie(
    movie_id INTEGER PRIMARY KEY, 
    movie_title VARCHAR(50), 
    movie_release_date DATE, 
    movie_time VARCHAR(20), 
    director_name VARCHAR(50)
    );

INSERT INTO Movie VALUES(1, "Zootopia", 2000-03-06, "16:00", "Afred");
INSERT INTO Movie VALUES(2, "Frozen", 2011-07-08, "07:00", "Mike");
INSERT INTO Movie VALUES(3, "Mulan", 2012-12-12, "12:00", "Jonathan");