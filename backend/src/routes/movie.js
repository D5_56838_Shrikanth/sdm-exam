const express= require "express"
const axios= require "axios"
const mysql= require "mysql2"
const createResult= require "../../utils"

const movie = ()=> {

    movie.get("/post/:id",request,response)= ()=> {
        const id= request.params

        const statement = "SELECT * FROM Movie WHERE id='$id'"
        mysql.get(statement, response) = ()=> {
            result= response
            return (createResult(result))
        }
    }

    movie.post("/get/",request,response)= ()=> {
        const {id, title, date, time, director}= request.body{id, title, date, time, director}

        const statement = "INSERT INTO Movie VALUES('$id','$title','$date','$time','$director')"
        mysql.post(statement, response) = ()=> {
            result= response
            return (createResult(result))
        }
    }

    movie.delete("/delete/:id",request,response)= ()=> {
        const id= request.params

        const statement = "DELETE * FROM Movie WHERE id='$id'"
        mysql.delete(statement, response) = ()=> {
            result= response
            return (createResult(result))
        }
    }

    movie.put("/update/:id",request,response)= ()=> {
        const id= request.params
        const {date, time}= request.body{date, time}

        const statement = "UPDATE Movie VALUES('$date','$time') WHERE id='$id'"
        mysql.put(statement, response) = ()=> {
            result= response
            return (createResult(result))
        }
    }
}

export.modules = movie